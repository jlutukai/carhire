package adapter;

public class PropertyGetSet {
    private String Transmission, Car_Seats,Image,Drive_Type,Location,Description,Car_Model,Latitude,Longitude;
    private Long Price;
    public PropertyGetSet(){}

    public PropertyGetSet(String transmission,String latitude,String longitude, String car_seats, String image, String drive_type, String location,Long price,
                          String description,String car_model) {
        Transmission = transmission;
        Car_Seats = car_seats;
        Image = image;
        Drive_Type = drive_type;
        Location = location;
        Price = price;
        Description = description;
        Car_Model = car_model;
        Latitude = latitude;
        Longitude = longitude;
    }


    public String getTransmission() {
        return Transmission;
    }

    public void setTransmission(String transmission) {
        Transmission = transmission;
    }

    public String getCar_Seats() {
        return Car_Seats;
    }

    public void setCar_Seats(String car_seats) {
        Car_Seats = car_seats;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDrive_Type() {
        return Drive_Type;
    }

    public void setDrive_Type(String drive_type) {
        Drive_Type = drive_type;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public Long getPrice() {
        return Price;
    }

    public void setPrice(Long price) {
        Price = price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }
    public String getCar_Model() {
        return Car_Model;
    }

    public void setCar_Model(String car_model) {
        Car_Model = car_model;
    }
}

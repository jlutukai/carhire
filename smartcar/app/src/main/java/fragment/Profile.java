package fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import adapter.PropertyGetSet;
import customfonts.MyTextView;
import de.hdodenhof.circleimageview.CircleImageView;
import io.supercharge.shimmerlayout.ShimmerLayout;
import com.smarthire.cars.R;
import com.smarthire.cars.UpdateProperty;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {
    FirebaseRecyclerAdapter adapter;
    private ShimmerLayout shimmerLayout;
    private CircleImageView circleImageView;
    private MyTextView name,mail;
    DatabaseReference myref,userDetails;
    public Profile() {
        // Required empty public constructor
    }
View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);

        shimmerLayout = view.findViewById(R.id.prof_shimmer_container);
        shimmerLayout.startShimmerAnimation();

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        String uid = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();

         myref = FirebaseDatabase.getInstance().getReference().child("Cars");
         userDetails = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
        myref.keepSynced(true);
        userDetails.keepSynced(true);

        RecyclerView recyclerView = view.findViewById(R.id.profile_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        name = view.findViewById(R.id.my_name);
        mail = view.findViewById(R.id.my_mail);

        userDetails.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String Name = dataSnapshot.child("Name").getValue().toString();
                String Email = dataSnapshot.child("Email").getValue().toString();

                name.setText(Name);
                mail.setText(Email);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        FirebaseRecyclerOptions<PropertyGetSet> options =
                new FirebaseRecyclerOptions.Builder<PropertyGetSet>()
                        .setQuery(myref.orderByChild(uid+1).equalTo("owner"), PropertyGetSet.class)
                        .build();
         adapter = new FirebaseRecyclerAdapter<PropertyGetSet, ProfileViewHolder>(options) {
            @NonNull
            @Override
            public ProfileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View s = LayoutInflater.from(getActivity())
                        .inflate(R.layout.profile_model, parent, false);

                return new ProfileViewHolder(s);
            }

            @Override
            protected void onBindViewHolder(@NonNull ProfileViewHolder holder, int position, @NonNull PropertyGetSet model) {
                final String Property_id = getRef(position).getKey();
                holder.setAddress(model.getLocation());
                holder.setTrans(model.getTransmission());
                holder.setCar_Seats(model.getCar_Seats());
                holder.setPrice(model.getPrice().toString());
                holder.setDrive_Type(model.getDrive_Type());
                holder.setCar_Model(model.getCar_Model());
                holder.setImageView(model.getImage());
                holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme)
                                .setTitle("Delete this post?")
                                .setMessage("Note that this action is irreversible")
                                .setIcon(R.drawable.ic_info_black_24dp)
                                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT).show();
                                        assert Property_id != null;
                                        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference().child("Cars").child(Property_id);
                                        myRef.removeValue();
                                    }
                                })
                                .setNegativeButton("Abort", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                })
                                .show();
                        return true;
                    }
                });

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new MaterialDialog.Builder(Objects.requireNonNull(getActivity()))
                                .title("Edit car post?")
                                .content("This will modify your already posted car\n Long press on card to delete this post")
                                .positiveText("Continue")
                                .negativeText("Close")
                               .iconRes(R.drawable.ic_info_black_24dp)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        Intent intent = new Intent(getActivity(), UpdateProperty.class);
                                        intent.putExtra("property_id",Property_id);
                                        startActivity(intent);
                                        getActivity().overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                                    }

                                })
                                .onNegative(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                    }
                                })
                                .show();
                    }
                });
// stop animating Shimmer and hide the layout
                shimmerLayout.stopShimmerAnimation();
                shimmerLayout.setVisibility(View.GONE);
            }
        };

        recyclerView.setAdapter(adapter);



        return view;

    }

    private class ProfileViewHolder extends RecyclerView.ViewHolder {

        View mView;
        TextView price,drive,car_type,trans,address,car_model;
        ImageView imageView;
        ProfileViewHolder(View itemView) {
            super(itemView);

            mView=itemView;
            price = itemView.findViewById(R.id.my_price);
            drive = itemView.findViewById(R.id.my_bed);
            car_type = itemView.findViewById(R.id.my_swimmingPool);
            trans = itemView.findViewById(R.id.my_shower);
            address = itemView.findViewById(R.id.my_location);
            imageView = itemView.findViewById(R.id.my_propPic);
            car_model = itemView.findViewById(R.id.my_propertyName);
        }

        public void setPrice(String Price){
            price.setText(Price);
        }
        private void setCar_Seats(String Car_Seats){
            drive.setText(Car_Seats);
        }
        private void setDrive_Type(String Drive_Type){
            car_type.setText(Drive_Type);
        }
        private void setTrans(String Trans){
            trans.setText(Trans);
        }
        private void setAddress(String Address){
            address.setText(Address);
        }
        private void setCar_Model(String Car_Model){
            car_model.setText(Car_Model);
        }
        private void setImageView(String image)
        {
            Picasso.with(itemView.getContext())
                    .load(image)
                    .into(imageView);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null){
            adapter.stopListening();
            shimmerLayout.stopShimmerAnimation();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (adapter != null){
            adapter.stopListening();
            shimmerLayout.stopShimmerAnimation();
        }
    }
}

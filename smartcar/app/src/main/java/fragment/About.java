package fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import adapter.PropertyGetSet;
import com.smarthire.cars.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class About extends Fragment {


    public About() {
        // Required empty public constructor
    }
    private DatabaseReference mUsers;
    private GoogleMap mMap;
    private Button btn;



View  view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_about, container, false);

        mUsers= FirebaseDatabase.getInstance().getReference().child("Cars");

        btn = view.findViewById(R.id.mapp);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mUsers.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot s : dataSnapshot.getChildren()){
                            PropertyGetSet property = s.getValue(PropertyGetSet.class);

                            String latitude = Objects.requireNonNull(property).getLatitude();
                            String longitude = Objects.requireNonNull(property).getLongitude();
                            String Prop_name = property.getCar_Model();

                            Double lat = Double.parseDouble(latitude);
                            Double longi = Double.parseDouble(longitude);


                            Uri gmmIntentUri = Uri.parse("geo:0,0?q="+lat+","+longi+"(" +Prop_name+")");
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                                startActivity(mapIntent);
                            }



                            LatLng location=new LatLng(lat,longi);
                            mMap.addMarker(new MarkerOptions().position(location).title(property.getCar_Model()))
                                    .setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));

                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,10));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });





        return view;
    }

}

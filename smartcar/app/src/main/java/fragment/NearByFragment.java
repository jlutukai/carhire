package fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;


import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import adapter.PropertyGetSet;
import io.supercharge.shimmerlayout.ShimmerLayout;
import com.smarthire.cars.PropertyDetails;
import com.smarthire.cars.QueryActivity;
import com.smarthire.cars.R;

import java.util.Objects;


/**
 * Created by Marcos on 10/10/2018.
 */

public class NearByFragment extends Fragment {

    View view;
    RecyclerView recyclerView;
    private DatabaseReference myref,favorites;
    private   FirebaseRecyclerAdapter adapter;
    private  FirebaseAuth mAuth;
    private ShimmerLayout shimmerLayout;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.nearby_houses, container, false);
        Objects.requireNonNull(getActivity()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        myref = FirebaseDatabase.getInstance().getReference().child("Cars");
        myref.keepSynced(true);

        favorites = FirebaseDatabase.getInstance().getReference().child("Favourite");
        favorites.keepSynced(true);
        shimmerLayout = view.findViewById(R.id.shimmer_view_container);
        shimmerLayout.startShimmerAnimation();
        recyclerView = view.findViewById(R.id.recyclerview_property);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        final EditText mSearchField = view.findViewById(R.id.search_field);
        mSearchField.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction()!=KeyEvent.ACTION_DOWN)
                    return false;
                if(keyCode == KeyEvent.KEYCODE_ENTER ){
                    final String query_text = mSearchField.getText().toString();
                    Intent intent = new Intent(getActivity(), QueryActivity.class);
                    intent.putExtra("Query_Text",query_text);
                    startActivity(intent);
                    Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                    return true;
                }
                return false;
            }
        });


        mAuth = FirebaseAuth.getInstance();
        FirebaseRecyclerOptions<PropertyGetSet> options =
                new FirebaseRecyclerOptions.Builder<PropertyGetSet>()
                        .setQuery(myref, PropertyGetSet.class)  //.orderByChild("Name").startAt(searchText).endAt(searchText + "\uf8ff")
                        .build();

        adapter = new FirebaseRecyclerAdapter<PropertyGetSet, PropertyViewHolder>(options) {
            @NonNull
            @Override
            public PropertyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View s = LayoutInflater.from(getActivity())
                        .inflate(R.layout.model_house, parent, false);

                return new PropertyViewHolder(s);
            }

            @Override
            protected void onBindViewHolder(@NonNull PropertyViewHolder holder, int position, @NonNull PropertyGetSet model) {
                setFadeAnimation(holder.itemView);
                final String Property_id = getRef(position).getKey();
                holder.setAddress(model.getLocation());
                holder.setTransmission(model.getTransmission());
                holder.setCar_Seats(model.getCar_Seats());
                holder.setCar_Model(model.getCar_Model());
                holder.setPrice(model.getPrice().toString());//Long.toString(model.getPrice())
                holder.setDrive_Type(model.getDrive_Type());
                holder.setImageView(model.getImage());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), PropertyDetails.class);
                        intent.putExtra("property_id",Property_id);
                        startActivity(intent);
                        Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                    }
                });

                // stop animating Shimmer and hide the layout
                shimmerLayout.stopShimmerAnimation();
                shimmerLayout.setVisibility(View.GONE);

            }
        };

        recyclerView.setAdapter(adapter);


        return view;

    }

    public void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null){
            adapter.stopListening();
            shimmerLayout.stopShimmerAnimation();
        }
    }
}

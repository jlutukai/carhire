package fragment;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import com.smarthire.cars.R;

class PropertyViewHolder extends RecyclerView.ViewHolder {
    View mView;
    TextView price,drive,car_type,trans,address, carmodel;
    ImageView imageView;

    PropertyViewHolder(View itemView) {
        super(itemView);
        mView=itemView;
        carmodel = itemView.findViewById(R.id.car_model);
        price = itemView.findViewById(R.id.price);
        drive = itemView.findViewById(R.id.bed);
        car_type = itemView.findViewById(R.id.model_pool);
        trans = itemView.findViewById(R.id.shower);
        address = itemView.findViewById(R.id.address);
        imageView = itemView.findViewById(R.id.mode_image);
    }

    public void setPrice(String Price){
        price.setText(Price);
    }
    public void setCar_Seats(String Car_Seats){
        drive.setText(Car_Seats);
    }
    public void setDrive_Type(String Drive_Type){
        car_type.setText(Drive_Type);
    }
    public void setTransmission(String Transmission){
        trans.setText(Transmission);
    }
    public void setAddress(String Address){
        address.setText(Address);
    }
    public void setCar_Model(String Car_Model){
        carmodel.setText(Car_Model);
    }
    public void setImageView(String image)
    {
        Picasso.with(itemView.getContext())
                .load(image)
                .into(imageView);
    }
}

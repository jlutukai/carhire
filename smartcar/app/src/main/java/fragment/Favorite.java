package fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import adapter.PropertyGetSet;
import io.supercharge.shimmerlayout.ShimmerLayout;
import com.smarthire.cars.PropertyDetails;
import com.smarthire.cars.R;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class Favorite extends Fragment {

    View view;
    RecyclerView recyclerView;
    private DatabaseReference myref,favorites;
    private FirebaseRecyclerAdapter adapter;
    private FirebaseAuth mAuth;
    private ShimmerLayout shimmerLayout;
    private FirebaseAuth firebaseAuth;
    private RelativeLayout layout;
    public Favorite() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.nearby_houses, container, false);

        layout= view.findViewById(R.id.tool);
        layout.setVisibility(View.INVISIBLE);


        firebaseAuth = FirebaseAuth.getInstance();
        String uid = firebaseAuth.getCurrentUser().getUid();

        myref = FirebaseDatabase.getInstance().getReference().child("Cars");
        myref.keepSynced(true);

        shimmerLayout = view.findViewById(R.id.shimmer_view_container);
        shimmerLayout.startShimmerAnimation();
        recyclerView = view.findViewById(R.id.recyclerview_property);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAuth = FirebaseAuth.getInstance();
        FirebaseRecyclerOptions<PropertyGetSet> options =
                new FirebaseRecyclerOptions.Builder<PropertyGetSet>()
                        .setQuery(myref.orderByChild(uid).equalTo("Liked"), PropertyGetSet.class)
                        .build();

        adapter = new FirebaseRecyclerAdapter<PropertyGetSet, PropertyViewHolder>(options) {
            @NonNull
            @Override
            public PropertyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View s = LayoutInflater.from(getActivity())
                        .inflate(R.layout.model_house, parent, false);

                return new PropertyViewHolder(s);
            }

            @Override
            protected void onBindViewHolder(@NonNull PropertyViewHolder holder, int position, @NonNull PropertyGetSet model) {

                final String Property_id = getRef(position).getKey();
                holder.setAddress(model.getLocation());
                holder.setTransmission(model.getTransmission());
                holder.setCar_Seats(model.getCar_Seats());
                holder.setPrice(model.getPrice().toString()
                );
                holder.setDrive_Type(model.getDrive_Type());
                holder.setImageView(model.getImage());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), PropertyDetails.class);
                        intent.putExtra("property_id",Property_id);
                        startActivity(intent);
                        Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                    }
                });


                // stop animating Shimmer and hide the layout
                shimmerLayout.stopShimmerAnimation();
                shimmerLayout.setVisibility(View.GONE);

            }
        };

        recyclerView.setAdapter(adapter);





        return  view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null){
            adapter.stopListening();
            shimmerLayout.stopShimmerAnimation();
        }
    }

}

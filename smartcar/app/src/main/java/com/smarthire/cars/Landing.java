package com.smarthire.cars;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import customfonts.MyTextView;

public class Landing extends AppCompatActivity implements View.OnClickListener {

private Button log_in,sign;
MyTextView textView;
    private long lastBackPressTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        log_in = findViewById(R.id.log_landing);
        sign = findViewById(R.id.sign_landing);
        log_in.setOnClickListener(this);
        sign.setOnClickListener(this);

        textView= findViewById(R.id.txt);
        textView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == log_in)
        {
            finish();
          startActivity(new Intent(Landing.this,SignIn.class));
          overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
        }
        if (v == sign)
        {
            finish();
            startActivity(new Intent(Landing.this,SignUp.class));
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
        }
        if (v == textView)
        {
          String showText = textView.getText().toString();
            Toast.makeText(this, showText, Toast.LENGTH_LONG).show();
            textView.setBackgroundResource(R.drawable.round_rectangle_blue);

        }

    }

    @Override
    public void onBackPressed() {

        if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            Toast.makeText(this, "Press again to exit House Hunt", Toast.LENGTH_SHORT).show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {

            super.onBackPressed();
        }
    }

}

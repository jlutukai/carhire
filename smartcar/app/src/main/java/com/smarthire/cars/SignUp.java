package com.smarthire.cars;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

import customfonts.MyEditText;
import customfonts.MyTextView;

public class SignUp extends AppCompatActivity implements View.OnClickListener {

    private MyTextView get_started,sign_in;
    private MyEditText name,mail,pass,pass2;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
//        FirebaseApp.initializeApp(this);

        get_started = findViewById(R.id.getstarted_sign);
        sign_in = findViewById(R.id.signin);
        name = findViewById(R.id.name_sign);
        mail = findViewById(R.id.mail_sign);
        pass = findViewById(R.id.pass_sign);
        pass2 = findViewById(R.id.pass_review);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Users");

        firebaseAuth = FirebaseAuth.getInstance();

        get_started.setOnClickListener(this);
        sign_in.setOnClickListener(this);
    }


    public void register() {

        final String Name = name.getText().toString().trim();
        final String Email = mail.getText().toString().trim();
        String password = pass.getText().toString().trim();
        String passwd = pass2.getText().toString().trim();



        if (TextUtils.isEmpty(Name)){
            name.setError("Input your name");
        }

        if (TextUtils.isEmpty(Name) || !android.util.Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
            mail.setError("Enter a valid email address");
            return;
        } else {
            mail.setError(null);
        }

        if (TextUtils.isEmpty(password) || password.length() < 6 || password.length() > 10) {
            pass.setError("between 6 and 10 alphanumeric characters");
            return;
        }
        if (passwd.length() < 4 || passwd.length() > 10 || !(passwd.equals(password))) {
            pass2.setError("Password Do not match");
            return;


        } else {
            pass2.setError(null);}


        final ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        firebaseAuth.createUserWithEmailAndPassword(Email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    {
//                        try {
//                            throw task.getException();
//                        }
//                        // if user enters wrong email.
//                        /*catch (FirebaseAuthWeakPasswordException weakPassword)
//                        {
//                            //Log.d(TAG, "onComplete: weak_password");
//
//                            // TODO: take your actions!
//                        }
//                        // if user enters wrong password.
//                        catch (FirebaseAuthInvalidCredentialsException malformedEmail)
//                        {
//                           // Log.d(TAG, "onComplete: malformed_email");
//
//                            // TODO: Take your action
//                        }*/ catch (FirebaseAuthUserCollisionException existEmail) {
//                            //Log.d(TAG, "onComplete: exist_email");
//                            mail.setError("User Exists!!");
//
//                        } catch (Exception e) {
//                            // Log.d(TAG, "onComplete: " + e.getMessage());
//                        }

                        String user_id = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
                        DatabaseReference current_user = databaseReference.child(user_id);
                        current_user.child("Name").setValue(Name);
                        current_user.child("Email").setValue(Email);

                        final ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
                        progressDialog.dismiss();
                        //start profile activity
                        finish();

                        Intent profile = new Intent(SignUp.this, Main.class);
                        startActivity(profile);
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    }
                } else {
                    Toast.makeText(SignUp.this, "connection timeout, check your internet connection", Toast.LENGTH_LONG).show();
                    final ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
                    progressDialog.dismiss();
                }
            }
        });

    }


    @Override
    public void onClick(View v) {
    if (v == get_started)
    {
       register();
    }
        if (v == sign_in)
        {
            finish();
            startActivity(new Intent(SignUp.this,SignIn.class));
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
        }

    }
}

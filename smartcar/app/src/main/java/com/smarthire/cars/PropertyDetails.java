package com.smarthire.cars;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;

import customfonts.MyTextView;
import ss.com.bannerslider.Slider;
import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class PropertyDetails extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {
        private DatabaseReference databaseReference,favorites;
        private String property_id = null;

        private ImageView imageView,favorite,back;
        MyTextView prop_name,location,furnish,prop_type,price,bed, bath,pool,quick,desc,phone,avail,name,
                gym, club, jog, play, tennis,security,gated,date;
        TextView call_agent;
        LinearLayout map,nav,street;
        public static String Lat, Longi,Prop_name,Phone ;
        private FirebaseAuth mAuth;
        private boolean mProcessFav = false;

        private Slider slider;
    String Image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house);
        property_id = getIntent().getExtras().getString("property_id");

        Slider.init(new PicassoImageLoadingService(this));

        slider = findViewById(R.id.banner_slider);
        slider.setAdapter(new SliderAdapterr());

        mAuth = FirebaseAuth.getInstance();
        final String uid = mAuth.getCurrentUser().getUid();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Cars").child(property_id);
        databaseReference.keepSynced(true);
//        favorites = FirebaseDatabase.getInstance().getReference().child("Favourite");
//        favorites.keepSynced(true);

        //imageView = findViewById(R.id.top_image);
        prop_name = findViewById(R.id.propertyName);
        prop_type = findViewById(R.id.prop_type);
        location = findViewById(R.id.address_location);
        furnish = findViewById(R.id.furnished);
        price = findViewById(R.id.amount);
        bed = findViewById(R.id.get_bed);
        bath = findViewById(R.id.get_shower);
        pool = findViewById(R.id.get_swim);
        quick = findViewById(R.id.quickpro);
        desc = findViewById(R.id.desc);
        phone =findViewById(R.id.phone);
        name =findViewById(R.id.get_name);
        avail =findViewById(R.id.available);
        call_agent =findViewById(R.id.call_agent);
        gym =findViewById(R.id.gym);
        club=findViewById(R.id.club);
        jog=findViewById(R.id.jog);
        play=findViewById(R.id.play);
        tennis=findViewById(R.id.tennis);
        security=findViewById(R.id.security);
        gated=findViewById(R.id.gated);
        date=findViewById(R.id.date);
        favorite=findViewById(R.id.favor);
        back=findViewById(R.id.back);
        back.setOnClickListener(this);

        map = findViewById(R.id.llDistance);
        nav = findViewById(R.id.navigation);
        street=findViewById(R.id.street);
        street.setOnClickListener(this);
        nav.setOnClickListener(this);
        map.setOnClickListener(this);
        call_agent.setOnClickListener(this);

        LikeButtonColor();


       favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProcessFav = true;
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (mProcessFav) {

                            if (dataSnapshot.hasChild(uid)){
                                databaseReference.child(uid).removeValue();
                               Toast.makeText(PropertyDetails.this, "Removed from  favourites", Toast.LENGTH_SHORT).show();
                                mProcessFav = false;
                            }
                            else {
                                databaseReference.child(uid).setValue("Liked");
                                Toast.makeText(PropertyDetails.this, "Saved to  favourites", Toast.LENGTH_SHORT).show();
                                mProcessFav = false;
                            }

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });






        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    Prop_name = (String) dataSnapshot.child("Car_Model").getValue();
                    String Prop_type = (String) dataSnapshot.child("Car_Type").getValue();
                    String Location = (String) dataSnapshot.child("Location").getValue();
                    String Furnish = (String) dataSnapshot.child("Area").getValue();
                    String Price = dataSnapshot.child("Price").getValue().toString();
                    String Bed = (String) dataSnapshot.child("Transmission").getValue();
                    String Bath = (String) dataSnapshot.child("Car_Seats").getValue();
                    String Pool = (String) dataSnapshot.child("Drive_Type").getValue();
                    String Quick = (String) dataSnapshot.child("Car_Reg").getValue();
                    String Desc = (String) dataSnapshot.child("Description").getValue();
                    Image = (String) dataSnapshot.child("Image").getValue();
                    Phone = (String) dataSnapshot.child("Phone").getValue();
                    String Available = (String) dataSnapshot.child("Driver").getValue();
                    String Name = (String) dataSnapshot.child("Name").getValue();
                    Long DatePosted = (Long)dataSnapshot.child("PostedDate") .getValue();
                     Lat = (String) dataSnapshot.child("Latitude") .getValue();
                     Longi = (String) dataSnapshot.child("Longitude").getValue();
                    String Gym = (String) dataSnapshot.child("Road").getValue();
                String Club = (String) dataSnapshot.child("Radio").getValue();
                String Jog = (String) dataSnapshot.child("Air_Conditioning").getValue();
                String Play = (String) dataSnapshot.child("Insuarance").getValue();
                String Tennis = (String) dataSnapshot.child("Alarm").getValue();
                String Security = (String) dataSnapshot.child("Service").getValue();
                String Gated = (String) dataSnapshot.child("Gated").getValue();

                if (Gym != null){
                    gym.setVisibility(View.VISIBLE);
                }
                if (Club != null){
                    club.setVisibility(View.VISIBLE);
                }
                if (Jog != null){
                    jog.setVisibility(View.VISIBLE);
                }
                if (Play != null){
                    play.setVisibility(View.VISIBLE);
                }
                if (Tennis != null){
                    tennis.setVisibility(View.VISIBLE);
                }
                if (Security != null){
                    security.setVisibility(View.VISIBLE);
                }
                if (Gated != null){
                    gated.setVisibility(View.VISIBLE);
                }


                    prop_name.setText(Prop_name);
                    prop_type.setText(Prop_type);
                    location.setText(Location);
                    furnish.setText(Furnish);
                    price.setText(Price);
                    bed.setText(Bed);
                    bath.setText(Bath);
                    pool.setText(Pool);
                    quick.setText(Quick);
                    desc.setText(Desc);
                    phone.setText(Phone);
                    avail.setText(Available);
                    name.setText(Name);
                    gym.setText(Gym);
                    club.setText(Club);
                    jog.setText(Jog);
                    play.setText(Play);
                    tennis.setText(Tennis);
                    security.setText(Security);
                    gated.setText(Gated);
                   // Picasso.with(PropertyDetails.this).load(Image).into(imageView);



                SimpleDateFormat simpleDateFormat =new SimpleDateFormat("dd-MM-yyyy");
                date.setText(simpleDateFormat.format(new Date(DatePosted)));




            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        // Get the SupportMapFragment and request notification
        // when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);




    }




    public class SliderAdapterr extends SliderAdapter {
       // String image2 = "https://assets.materialup.com/uploads/20ded50d-cc85-4e72-9ce3-452671cf7a6d/preview.jpg";
        @Override
        public int getItemCount() {
            return 3;
        }

        @Override
        public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
            switch (position) {
                case 0:
//                    viewHolder.bindImageSlide("https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg");
                    viewHolder.bindImageSlide("https://firebasestorage.googleapis.com/v0/b/house-hunt-1529311286276.appspot.com/o/custom_uploads%2F1.jpeg?alt=media&token=4ad80d05-ea87-4a2b-bdc5-9815f4241459");
                    break;
                case 1:
                    viewHolder.bindImageSlide(Image);
                    break;
                case 2:
                  //  viewHolder.bindImageSlide("https://assets.materialup.com/uploads/76d63bbc-54a1-450a-a462-d90056be881b/preview.png");
                   viewHolder.bindImageSlide("https://firebasestorage.googleapis.com/v0/b/house-hunt-1529311286276.appspot.com/o/custom_uploads%2F1.jpeg?alt=media&token=4ad80d05-ea87-4a2b-bdc5-9815f4241459");
                    break;
            }
        }
    }

    private void LikeButtonColor() {
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(mAuth.getCurrentUser().getUid()))
                {
                    favorite.setImageResource(R.drawable.ic_favorite_yellow);
                }
                else
                {
                    favorite.setImageResource(R.drawable.ic_favorite_black_24dp);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        if (Lat != null){
            Double lt = Double.parseDouble(Lat);
            Double lg = Double.parseDouble(Longi);

            LatLng Prop_location = new LatLng(lt,lg);
            googleMap.addMarker(new MarkerOptions().position(Prop_location)
                    .title("Property Location"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Prop_location,10));

        }
        else {
            Toast.makeText(this, "map not loaded", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View view) {
        if (view == map)
        {
            if (Lat != null){
                Double lt = Double.parseDouble(Lat);
                Double lg = Double.parseDouble(Longi);
                Uri gmmIntentUri = Uri.parse("geo:0,0?q="+lt+","+lg+"(" +Prop_name+")");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }

            }
            else {
                Toast.makeText(this, "map not loaded", Toast.LENGTH_SHORT).show();
            }
        }
        if (view == nav)
        {

            if (Lat != null){
                Double lt = Double.parseDouble(Lat);
                Double lg = Double.parseDouble(Longi);

                Uri gmmIntentUri = Uri.parse("google.navigation:q="+lt+","+lg);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }

            }
            else {
                Toast.makeText(this, "map not loaded", Toast.LENGTH_SHORT).show();
            }



        }
        if (view == street)
        {


            if (Lat != null){
                Double lt = Double.parseDouble(Lat);
                Double lg = Double.parseDouble(Longi);
                Uri gmmIntentUri = Uri.parse("google.streetview:cbll"+lt+","+lg);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }

            }
            else {
                Toast.makeText(this, "map not loaded", Toast.LENGTH_SHORT).show();
            }



        }

        if (view == back)
        {
            startActivity(new Intent(PropertyDetails.this, Main.class));
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
        }
        if (view == call_agent)
        {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            // Send phone number to intent as data
            intent.setData(Uri.parse("tel:" +Phone));
            // Start the dialer app activity with number
            startActivity(intent);
        }
    }
}

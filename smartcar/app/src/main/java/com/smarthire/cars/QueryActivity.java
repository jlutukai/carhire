package com.smarthire.cars;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.Objects;

import adapter.PropertyGetSet;
import io.supercharge.shimmerlayout.ShimmerLayout;

public class QueryActivity extends AppCompatActivity {
    private DatabaseReference myref,ref;
    private FirebaseRecyclerAdapter adapter;
    private FirebaseAuth mAuth;
    private ShimmerLayout shimmerLayout;
    RecyclerView recyclerView;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);

        String query_text = Objects.requireNonNull(getIntent().getExtras()).getString("Query_Text");
        Long ttt = Long.parseLong(query_text);

        myref = FirebaseDatabase.getInstance().getReference().child("Cars");
        myref.keepSynced(true);
        mAuth = FirebaseAuth.getInstance();


        shimmerLayout = findViewById(R.id.shimmer_container);
        shimmerLayout.startShimmerAnimation();
        recyclerView = findViewById(R.id.query_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        button = findViewById(R.id.sort);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                GeoFireSort();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        FirebaseRecyclerOptions<PropertyGetSet> options =
                new FirebaseRecyclerOptions.Builder<PropertyGetSet>()
                        .setQuery(myref.orderByChild("Price").endAt(ttt), PropertyGetSet.class)//.orderByChild("Price").startAt(ttt).endAt(ttt + "\uf8ff")
                        .build();
        adapter = new FirebaseRecyclerAdapter<PropertyGetSet, QueryViewHolder>(options) {
            @NonNull
            @Override
            public QueryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                View s = LayoutInflater.from(QueryActivity.this)
                        .inflate(R.layout.model_house, parent, false);

                return new QueryViewHolder(s);
            }

            @Override
            protected void onBindViewHolder(@NonNull QueryViewHolder holder, int position, @NonNull PropertyGetSet model) {
                final String Property_id = getRef(position).getKey();
                holder.setAddress(model.getLocation());
                holder.setTransmission(model.getTransmission());
                holder.setCar_Seats(model.getCar_Seats());
                holder.setPrice(model.getPrice().toString());
                holder.setDrive_Type(model.getDrive_Type());
                holder.setImageView(model.getImage());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(QueryActivity.this, PropertyDetails.class);
                        intent.putExtra("property_id",Property_id);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                    }
                });


                // stop animating Shimmer and hide the layout
                shimmerLayout.stopShimmerAnimation();
                shimmerLayout.setVisibility(View.GONE);

            }
        };

        recyclerView.setAdapter(adapter);

    }

    private class QueryViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView price,drive,car_type,trans,address;
        ImageView imageView;

        private QueryViewHolder(View itemView) {
            super(itemView);
            mView=itemView;
            price = itemView.findViewById(R.id.price);
            drive = itemView.findViewById(R.id.bed);
            car_type = itemView.findViewById(R.id.model_pool);
            trans = itemView.findViewById(R.id.shower);
            address = itemView.findViewById(R.id.address);
            imageView = itemView.findViewById(R.id.mode_image);
        }

        public void setPrice(String Price){
            price.setText(Price);
        }
        void setCar_Seats(String Car_Seats){
            drive.setText(Car_Seats);
        }
         void setDrive_Type(String Drive_Type){
            car_type.setText(Drive_Type);
        }
         void setTransmission(String Transmission){
            trans.setText(Transmission);
        }
         void setAddress(String Address){
            address.setText(Address);
        }
         void setImageView(String image)
        {
            Picasso.with(itemView.getContext())
                    .load(image)
                    .into(imageView);
        }
    }

    public void GeoFireSort()
    {
        Double userLatitde = -1.373552;
        Double userLongitude = 36.769275;
        ref = FirebaseDatabase.getInstance().getReference().child("GeoFire");
        GeoFire geoFire = new GeoFire(ref);
        GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(userLatitde, userLongitude), 3);
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                //Any location key which is within 3km from the user's location will show up here as the key parameter in this method
                //You can fetch the actual data for this location by creating another firebase query here
  /*              myref.orderByChild(key).equalTo("owner").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        //The dataSnapshot should hold the actual data about the location
//                        dataSnapshot.get
//                        dataSnapshot.getChild("name").getValue(String.class); //should return the name of the location and dataSnapshot.getChild("description").getValue(String.class); //should return the description of the locations
                        String Prop_name = (String) dataSnapshot.child("Property_name").getValue();
                        String Prop_type = (String) dataSnapshot.child("PropertyType").getValue();
                        String Price = dataSnapshot.child("Price").getValue().toString();

                    }
//.setQuery(myref.orderByChild(uid).equalTo("Liked"),
                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });*/
                FirebaseRecyclerOptions<PropertyGetSet> options =
                        new FirebaseRecyclerOptions.Builder<PropertyGetSet>()
                                .setQuery(myref.orderByChild(key).equalTo("owner"), PropertyGetSet.class)//.orderByChild("Price").startAt(ttt).endAt(ttt + "\uf8ff")
                                .build();
                adapter = new FirebaseRecyclerAdapter<PropertyGetSet, QueryViewHolder>(options) {
                    @NonNull
                    @Override
                    public QueryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

                        View s = LayoutInflater.from(QueryActivity.this)
                                .inflate(R.layout.model_house, parent, false);

                        return new QueryViewHolder(s);
                    }

                    @Override
                    protected void onBindViewHolder(@NonNull QueryViewHolder holder, int position, @NonNull PropertyGetSet model) {
                        final String Property_id = getRef(position).getKey();
                        holder.setAddress(model.getLocation());
                        holder.setTransmission(model.getTransmission());
                        holder.setCar_Seats(model.getCar_Seats());
                        holder.setPrice(model.getPrice().toString());
                        holder.setDrive_Type(model.getDrive_Type());
                        holder.setImageView(model.getImage());
                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent intent = new Intent(QueryActivity.this, PropertyDetails.class);
                                intent.putExtra("property_id",Property_id);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
                            }
                        });


                        // stop animating Shimmer and hide the layout
                        shimmerLayout.stopShimmerAnimation();
                        shimmerLayout.setVisibility(View.GONE);

                    }
                };

                recyclerView.setAdapter(adapter);


            }

            @Override
            public void onKeyExited(String key) {}

            @Override
            public void onKeyMoved(String key, GeoLocation location) {}

            @Override
            public void onGeoQueryReady() {
                //This method will be called when all the locations which are within 3km from the user's location has been loaded Now you can do what you wish with this data
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.startListening();
            shimmerLayout.startShimmerAnimation();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (adapter != null){
            adapter.stopListening();
            shimmerLayout.stopShimmerAnimation();
        }
    }
}

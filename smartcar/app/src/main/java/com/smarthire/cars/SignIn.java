package com.smarthire.cars;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;

import customfonts.MyEditText;
import customfonts.MyTextView;

public class SignIn extends AppCompatActivity implements View.OnClickListener {
    private MyTextView reset, sign, get_started;
    private MyEditText mail, passwordd;

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        firebaseAuth = FirebaseAuth.getInstance();
        if (firebaseAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(getApplicationContext(), Main.class));
            //start profile activity
        }

        reset = findViewById(R.id.reset);
        sign = findViewById(R.id.sign_back);
        get_started = findViewById(R.id.getstarted);
        mail = findViewById(R.id.mail_log);
        passwordd = findViewById(R.id.pass_log);

       get_started.setOnClickListener(this);
       reset.setOnClickListener(this);
       sign.setOnClickListener(this);
    }

    public void login() {


        String email = mail.getText().toString().trim();
        final String password = passwordd.getText().toString().trim();

        if (TextUtils.isEmpty(email) || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mail.setError("enter a valid email address");
            return;
        } else {
            mail.setError(null);
        }

        if (password.isEmpty() || password.length() < 6 || password.length() > 10) {
            passwordd.setError("between 6 and 10 alphanumeric characters");
        }
        else {
            passwordd.setError(null);}


        final ProgressDialog progressDialog = new ProgressDialog(SignIn.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Validating...");
        progressDialog.show();


        firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    {
                        try {
                            throw task.getException();
                        }
                        // if user enters wrong email.
                        catch (FirebaseAuthInvalidUserException invalidEmail) {
                            // Log.d(TAG, "onComplete: invalid_email");
                            mail.setError("User does not exist!");
                            progressDialog.dismiss();

                            // TODO: take your actions!
                        }
                        // if user enters wrong password.
                        catch (FirebaseAuthInvalidCredentialsException wrongPassword) {
                            //Log.d(TAG, "onComplete: wrong_password");
                            passwordd.setError("Check your password");
                            progressDialog.dismiss();

                            // TODO: Take your action
                        } catch (Exception e) {
                            //  Log.d(TAG, "onComplete: " + e.getMessage());
                        }


                        finish();
                        //start profile activity

                        startActivity(new Intent(getApplicationContext(), Main.class));
                        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                    }
                    } else{
                        Toast.makeText(SignIn.this, "connection timeout", Toast.LENGTH_LONG).show();
                         progressDialog.dismiss();



                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v == reset)
        {
            startActivity(new Intent(SignIn.this,Reset.class));
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            finish();
        }
        if (v == sign)
        {
            startActivity(new Intent(SignIn.this,SignUp.class));
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            finish();
        }
        if (v == get_started)
        {
            login();
        }
    }
}

package com.smarthire.cars;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class Post_Property extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener{
    private ImageButton img_car;
    private static final int GALLERY_REQUEST=1;
    private Uri img_uri=null;
    // private String Bath,Bed,Swim,Type,Furnished;

    //Google maps placepicker
    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 1;
    private TextView placeDetails,location;
    private CircleImageView pick_place;

    private EditText car_model, phone, name, car_reg,desc,price;
    private CheckBox offroad, radio, aircon, insurance, alarm,service;
    Spinner drive,cartype,trans,area,driver,seat;
    Button post;
    private TextView lat,longi;

    private StorageReference storageReference;
    private DatabaseReference databaseReference,reference;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post__property);

        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Cars");
        reference = FirebaseDatabase.getInstance().getReference().child("Users");


        firebaseAuth = FirebaseAuth.getInstance();

        img_car = findViewById(R.id.property_image_post);
        img_car.setOnClickListener(this);

        car_model = findViewById(R.id.post_prop_name);
        location = findViewById(R.id.post_location);
        phone = findViewById(R.id.post_phone);
        name = findViewById(R.id.post_name);
        cartype = findViewById(R.id.post_cartype);
        car_reg = findViewById(R.id.post_quickpro);
        desc = findViewById(R.id.post_description);
        offroad = findViewById(R.id.post_gym);
        radio = findViewById(R.id.post_club);
        aircon = findViewById(R.id.post_jog);
        insurance = findViewById(R.id.post_play);
        alarm = findViewById(R.id.post_serviced);
        service = findViewById(R.id.post_security);
        price = findViewById(R.id.post_price);
        post = findViewById(R.id.post_property);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PostToDatabase();
            }
        });

        drive = findViewById(R.id.drive);
        seat = findViewById(R.id.spin_seat);
        trans = findViewById(R.id.spin_trans);
        area = findViewById(R.id.post_area);
        driver = findViewById(R.id.post_drive);
        lat = findViewById(R.id.latitude);
        longi = findViewById(R.id.longitude);


        ArrayAdapter<CharSequence> adapter_bed = ArrayAdapter.createFromResource(this,R.array.drive_array,R.layout.spinner_item);
        adapter_bed.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        drive.setAdapter(adapter_bed);

        ArrayAdapter<CharSequence> adapter_shower = ArrayAdapter.createFromResource(this,R.array.transmission_array,R.layout.spinner_item);
        adapter_shower.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        trans.setAdapter(adapter_shower);

        ArrayAdapter<CharSequence> adapter_swim = ArrayAdapter.createFromResource(this,R.array.seat_array,R.layout.spinner_item);
        adapter_swim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        seat.setAdapter(adapter_swim);

        ArrayAdapter<CharSequence> adapter_furnish = ArrayAdapter.createFromResource(this,R.array.area,R.layout.spinner_item);
        adapter_swim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        area.setAdapter(adapter_furnish);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.driver,R.layout.spinner_item);
        adapter_swim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        driver.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter_state = ArrayAdapter.createFromResource(this,R.array.cartype_array,R.layout.spinner_item);
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cartype.setAdapter(adapter_state);

        //place picker implementation
        pick_place = findViewById(R.id.place_picker);
        placeDetails = findViewById(R.id.feedback);

        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();


        pick_place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(Post_Property.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void PostToDatabase() {

        //get data from user/car owner
        final String Car_Model = car_model.getText().toString().trim();
        final String Location = location.getText().toString().trim();
        final String Phone = phone.getText().toString().trim();
        final String Name = name.getText().toString().trim();
        final String Seat = seat.getSelectedItem().toString().trim();
        final String Car_Reg = car_reg.getText().toString().trim();
        final String OffRoad = offroad.getText().toString().trim();
        final String Radio = radio.getText().toString().trim();
        final String Aircon = aircon.getText().toString().trim();
        final String Insured = insurance.getText().toString().trim();
        final String Alarm = alarm.getText().toString().trim();
        final String Serviced = service.getText().toString().trim();
        final String Description = desc.getText().toString().trim();
        final String Car_Type = cartype.getSelectedItem().toString().trim();
        final String Trans = trans.getSelectedItem().toString().trim();
        final String Drive = drive.getSelectedItem().toString().trim();
        final String Driver = driver.getSelectedItem().toString().trim();
        final String Area = area.getSelectedItem().toString().trim();
        final String cost = price.getText().toString().trim();
        final Long Price = Long.parseLong(cost);
        final String Latt = lat.getText().toString().trim();
        final String Longi = longi.getText().toString().trim();
        final Double latitude = Double.parseDouble(Latt);
        final Double longitude = Double.parseDouble(Longi);
        //check for empty entries and post to firebase database
        if (!TextUtils.isEmpty(Car_Model)&& !TextUtils.isEmpty(Location)&& !TextUtils.isEmpty(Phone)&& !TextUtils.isEmpty(Name)&& !TextUtils.isEmpty(Seat)&&
                !TextUtils.isEmpty(Car_Reg)&&img_uri !=null)
        {
//            Toast.makeText(getApplicationContext(), Drive,Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), Driver,Toast.LENGTH_LONG).show();
//            Toast.makeText(getApplicationContext(), Trans,Toast.LENGTH_LONG).show();
            //show dialog
            final ProgressDialog progressDialog = new ProgressDialog(Post_Property.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Posting your car online...");
            progressDialog.show();

            final StorageReference filepath = storageReference.child("Car_images").child(Objects.requireNonNull(img_uri.getLastPathSegment()));
            // Get the data from an ImageView as bytes
            img_car.setDrawingCacheEnabled(true);
            img_car.buildDrawingCache();
            Bitmap bitmap = ((BitmapDrawable) img_car.getDrawable()).getBitmap();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] data = baos.toByteArray();

            final UploadTask uploadTask = filepath.putBytes(data);
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {


                    final Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }


                            // Continue with the task to get the download URL
                            return filepath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                String user_id = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
                                DatabaseReference post = databaseReference.push();

                                //global
                                post.child("Car_Model").setValue(Car_Model);
                                post.child("Location").setValue(Location);
                                post.child("Phone").setValue(Phone);
                                post.child("Name").setValue(Name);
                                post.child("Car_Seats").setValue(Seat);
                                post.child("Car_Reg").setValue(Car_Reg);
                                post.child("Description").setValue(Description);
                                post.child("Car_Type").setValue(Car_Type);
                                post.child("Area").setValue(Area);
                                post.child("Transmission").setValue(Trans);
                                post.child("Driver").setValue(Driver);
                                post.child("Drive_Type").setValue(Drive);
                                post.child("Price").setValue(Price);
                                post.child("Latitude").setValue(Latt);
                                post.child("Longitude").setValue(Longi);
                                post.child("Image").setValue(downloadUri.toString());
                                post.child("PostedDate").setValue(ServerValue.TIMESTAMP);
                                post.child("Boolean").setValue(1);
                                post.child(user_id+1).setValue("owner");
                                DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("GeoFire");
                                GeoFire geoFire = new GeoFire(db);
                                geoFire.setLocation(user_id,new GeoLocation(latitude,longitude), new GeoFire.CompletionListener() {
                                    @Override
                                    public void onComplete(String key, DatabaseError error) {
                                        if (error != null) {
                                            System.err.println("There was an error saving the location to GeoFire: " + error);
                                        } else {
                                            System.out.println("Location saved on server successfully!");
                                        }
                                    }


                                });


                                if (offroad.isChecked())
                                {
                                    post.child("Road").setValue(OffRoad);
                                }
                                if (radio.isChecked())
                                {
                                    post.child("Radio").setValue(Radio);
                                }
                                if (aircon.isChecked())//gym, club, jog, play, tennis,security,gated;gym, club, jog, play, tennis,security,gated;
                                {
                                    post.child("Air_Conditioning").setValue(Aircon);
                                }
                                if (insurance.isChecked())
                                {
                                    post.child("Insuarance").setValue(Insured);
                                }
                                if (alarm.isChecked())
                                {
                                    post.child("Alarm").setValue(Alarm);
                                }
                                if (service.isChecked())
                                {
                                    post.child("Service").setValue(Serviced);
                                }


                                finish();
                                startActivity(new Intent(Post_Property.this,Main.class));



                            } else {
                                // Handle failures

                            }
                        }
                    });
                }
            });
        }
        else{
            car_model.setError("Form not fully filled!");
        }
    }
    @Override
    public void onClick(View v) {
        if (v == img_car )
        {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setActivityTitle("Crop Image")
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    //.setRequestedSize(400,200)
                    .start(this);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode ==RESULT_OK && data !=null){
                img_uri = result.getUri();
                //Picasso.with(this).load(img_uri).into(img_property);
                img_car.setImageURI(img_uri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                Toast.makeText(this, "Cropping Failed"+result.getError() ,Toast.LENGTH_SHORT).show();
            }
        }


            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);

                if (resultCode == RESULT_OK && data !=null) {
                    img_uri = result.getUri();
                    img_car.setImageURI(img_uri);
                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                    Toast.makeText(this, "Cropping failed"+result.getError(), Toast.LENGTH_SHORT).show();
                }
            }



        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Longitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                placeDetails.setText(stBuilder.toString());
                lat.setText(latitude);
                longi.setText(longitude);
                location.setText(address);
            }
        }


    }

    //maps continued
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this, connectionResult.getErrorMessage() +"", Toast.LENGTH_SHORT).show();
    }

}

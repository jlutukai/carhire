package com.smarthire.cars;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.rupins.drawercardbehaviour.CardDrawerLayout;

import java.util.Objects;

import fragment.Favorite;
import fragment.NearByFragment;
import fragment.Profile;


public class Main extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener  {

    private CardDrawerLayout drawer;
    private TextView name,email;
    private   NavigationView navigationView;
    Toolbar toolbar;
    private FirebaseAuth firebaseAuth;
    private  DatabaseReference db;

    private long lastBackPressTime = 0;

    BottomBar bottomBar;
    private int selectedTabId;

    // index to identify current nav menu item
    public static int navItemIndex = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        firebaseAuth = FirebaseAuth.getInstance();
        String user = Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid();
        if (firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, Landing.class));
        }

         drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setRadius(Gravity.START, 35);
        drawer.setViewElevation(Gravity.START, 20);


        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        View header = navigationView.getHeaderView(0);

         name = header.findViewById( R.id.name_nav);
         email = header.findViewById(R.id.email_nav);

        FirebaseUser user1 = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name
            assert user1 != null;
            String Email = user1.getEmail();
            email.setText(Email);
        }


            db = FirebaseDatabase.getInstance().getReference().child("Users").child(user);
         db.addListenerForSingleValueEvent(new ValueEventListener() {
             @Override
             public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                 String Name = Objects.requireNonNull(dataSnapshot.child("Name").getValue()).toString();
                 String Email = Objects.requireNonNull(dataSnapshot.child("Email").getValue()).toString();

                 name.setText(Name);
//                 email.setText(Email);
             }

             @Override
             public void onCancelled(@NonNull DatabaseError databaseError) {

             }
         });
//



        bottomBar = findViewById(R.id.bottombar);
        for (int i = 0; i < bottomBar.getTabCount(); i++) {
            bottomBar.getTabAtPosition(i).setGravity(Gravity.CENTER_VERTICAL);
        }

       //roughike bottombar library code is here

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {

            @Override
            public void onTabSelected(@IdRes int tabId) {

                switch (tabId) {
                    case R.id.recent:
                        selectedTabId =R.id.recent;
                        replace_fragment(new NearByFragment());
                        toolbar.setTitle("All Cars");

                        break;

                    case R.id.schedule:
                        selectedTabId =R.id.schedule;
                        replace_fragment(new Profile());
                        toolbar.setTitle("My Profile");

                        break;

                    case R.id.favorite:
                        selectedTabId = R.id.favorite;
                        toolbar.setTitle("Saved Cars");
                        replace_fragment(new Favorite());
                        break;

                    case R.id.post:
                        selectedTabId =R.id.post;
                        startActivity(new Intent(Main.this,Post_Property.class));
                        toolbar.setTitle("Post Your Car");

                        break;
                    case R.id.more:
                        selectedTabId =R.id.more;
                        replace_fragment(new NearByFragment());

                        break;
                        default:
                            new NearByFragment();

                }
            }
        });

    }



    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        else if (this.lastBackPressTime < System.currentTimeMillis() - 4000) {
            Toast.makeText(this, "Press again to exit Smart carhire app", Toast.LENGTH_SHORT).show();
            this.lastBackPressTime = System.currentTimeMillis();
        } else {

            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        if (navItemIndex == 0) {
            getMenuInflater().inflate(R.menu.main, menu);
        }

        // when fragment is notifications, load the menu created for notifications
        if (navItemIndex == 4) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(Main.this, Landing.class));
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            return true;
        }

        // user is in notifications fragment
        // and selected 'Mark all as Read'
        if (id == R.id.action_mark_all_read) {
            Toast.makeText(getApplicationContext(), "All notifications marked as read!", Toast.LENGTH_LONG).show();
        }

        // user is in notifications fragment
        // and selected 'Clear All'
        if (id == R.id.action_clear_notifications) {
            Toast.makeText(getApplicationContext(), "Clear all notifications!", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
            replace_fragment(new NearByFragment());
            toolbar.setTitle("All Cars");

        } else if (id == R.id.nav_fav) {
            replace_fragment(new Favorite());
            toolbar.setTitle("Saved Cars");

        } else if (id == R.id.nav_search) {
            Toast.makeText(this, "To add more search features", Toast.LENGTH_SHORT).show();

        } else if (id == R.id.nav_post) {
            startActivity(new Intent(Main.this,Post_Property.class));

        } else if (id == R.id.nav_notifications) {
            Toast.makeText(this, "To add notification settings", Toast.LENGTH_SHORT).show();
           // toolbar.setTitle("Notifications");

        } else if (id == R.id.nav_map) {
            startActivity(new Intent(this,PropertyMap.class));

        }else if (id == R.id.nav_about_us) {
           // startActivity(new Intent(Main.this,RecyclerView_implement.class));

        }else if (id == R.id.nav_logout) {
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(Main.this, Landing.class));
            overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);

        }


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void replace_fragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();
    }



}

package com.smarthire.cars;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import customfonts.MyEditText;
import customfonts.MyTextView;

public class Reset extends AppCompatActivity {
    private Button btn_back;
    private MyEditText mail;
    private MyTextView reset;
    ProgressBar progressBar;

    private  FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);
        auth = FirebaseAuth.getInstance();

        btn_back = findViewById(R.id.btn_back);
        mail = findViewById(R.id.mail_reset);
        reset = findViewById(R.id.reset_button);
        progressBar = findViewById(R.id.progressBar);



        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mail.getText().toString().trim();

                if(TextUtils.isEmpty(email)){
                    Toast.makeText(getApplicationContext(),"Enter your registered email",Toast.LENGTH_LONG).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(Reset.this,"We have sent you instructions to reset your password!",Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(Reset.this,"Failed to send reset email!",Toast.LENGTH_LONG).show();
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                });

                btn_back.setVisibility(View.VISIBLE);
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Reset.this,SignIn.class);
                startActivity(intent);
            }
        });
    }
}


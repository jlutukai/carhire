package com.smarthire.cars;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class UpdateProperty extends AppCompatActivity {
    private EditText phone,desc,price;
    private CheckBox gym, club, jog, play, tennis,security,gated;
    Spinner bed,swim,shower,furnish,property_type, state;
    ImageView imageView;
    Button postBtn;

    private DatabaseReference post;
    public  String property_id = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_property);

        property_id = Objects.requireNonNull(getIntent().getExtras()).getString("property_id");

        post = FirebaseDatabase.getInstance().getReference().child("Cars").child(property_id);
        post.keepSynced(true);
        post.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String image = Objects.requireNonNull(dataSnapshot.child("Image").getValue()).toString();
                Picasso.with(UpdateProperty.this)
                        .load(image)
                        .into(imageView);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        phone = findViewById(R.id.update_phone);
        state = findViewById(R.id.update_state);
        desc = findViewById(R.id.update_description);
        gym = findViewById(R.id.update_gym);
        club = findViewById(R.id.update_club);
        jog = findViewById(R.id.update_jog);
        play = findViewById(R.id.update_play);
        tennis = findViewById(R.id.update_tennis);
        security = findViewById(R.id.update_security);
        gated = findViewById(R.id.update_gated);
        price = findViewById(R.id.update_price);
        postBtn = findViewById(R.id.update_property);
        postBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateDb();
            }
        });

        bed = findViewById(R.id.update_bed);
        swim = findViewById(R.id.update_swim);
        shower = findViewById(R.id.update_shower);
        furnish = findViewById(R.id.update_furnished);
        property_type = findViewById(R.id.update_prop_type);
        imageView = findViewById(R.id.update_image);


        ArrayAdapter<CharSequence> adapter_bed = ArrayAdapter.createFromResource(this,R.array.drive_array,R.layout.spinner_item);
        adapter_bed.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bed.setAdapter(adapter_bed);

        ArrayAdapter<CharSequence> adapter_shower = ArrayAdapter.createFromResource(this,R.array.transmission_array,
                R.layout.spinner_item);
        adapter_shower.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        shower.setAdapter(adapter_shower);

        ArrayAdapter<CharSequence> adapter_swim = ArrayAdapter.createFromResource(this,R.array.cartype_array,
                R.layout.spinner_item);
        adapter_swim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        swim.setAdapter(adapter_swim);

        ArrayAdapter<CharSequence> adapter_furnish = ArrayAdapter.createFromResource(this,R.array.area,
                R.layout.spinner_item);
        adapter_swim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        furnish.setAdapter(adapter_furnish);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.driver,
                R.layout.spinner_item);
        adapter_swim.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        property_type.setAdapter(adapter);

    }

    private void UpdateDb() {
        //get data from user/landlord
        final String Phone = phone.getText().toString().trim();
        final String Vacant = state.getSelectedItem().toString().trim();
        final String Gym = gym.getText().toString().trim();
        final String Club = club.getText().toString().trim();
        final String Jog = jog.getText().toString().trim();
        final String Play = play.getText().toString().trim();
        final String Tennis = tennis.getText().toString().trim();
        final String Security = security.getText().toString().trim();
        final String Gated = gated.getText().toString().trim();
        final String Description = desc.getText().toString().trim();
        final String Swim = swim.getSelectedItem().toString().trim();
        final String Bath = shower.getSelectedItem().toString().trim();
        final String Bed = bed.getSelectedItem().toString().trim();
        final String Type = property_type.getSelectedItem().toString().trim();
        final String Furnished = furnish.getSelectedItem().toString().trim();
        final String cost = price.getText().toString().trim();
        final Long Price = Long.parseLong(cost);

        //check for empty entries and post to firebase database
        if (!TextUtils.isEmpty(Phone)&& !TextUtils.isEmpty(Vacant))
        { phone.setError(null);
        }
        else {
            phone.setError("Cannot be empty");
            return;
        }
            //show dialog
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Updating your property...");
            progressDialog.show();

            post.child("Phone").setValue(Phone);
            post.child("VacantToLet").setValue(Vacant);
            post.child("Description").setValue(Description);
            post.child("Pools").setValue(Swim);
            post.child("Furnished").setValue(Furnished);
            post.child("Bathrooms").setValue(Bath);
            post.child("PropertyType").setValue(Type);
            post.child("Bedrooms").setValue(Bed);
            post.child("Price").setValue(Price);
            post.child("PostedDate").setValue(ServerValue.TIMESTAMP);


            if (gym.isChecked())
            {
                post.child("Gym").setValue(Gym);
            }
            if (club.isChecked())
            {
                post.child("ClubHouse").setValue(Club);
            }
            if (jog.isChecked())//gym, club, jog, play, tennis,security,gated;gym, club, jog, play, tennis,security,gated;
            {
                post.child("JoggingTrack").setValue(Jog);
            }
            if (play.isChecked())
            {
                post.child("PlayGround").setValue(Play);
            }
            if (tennis.isChecked())
            {
                post.child("TennisCourt").setValue(Tennis);
            }
            if (security.isChecked())
            {
                post.child("Security").setValue(Security);
            }
            if (gated.isChecked())
            {
                post.child("Gated").setValue(Gated);
            }
            Toast.makeText(this, "Updated successfully", Toast.LENGTH_SHORT).show();
            finish();
            startActivity(new Intent(this,Main.class));
           overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);

    }

}

package com.smarthire.cars;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ss.com.bannerslider.Slider;

public class TestAct extends AppCompatActivity {
private Slider slider;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        Slider.init(new PicassoImageLoadingService(this));

        slider = findViewById(R.id.banner_slider1);
        slider.setAdapter(new MainSliderAdapter());
    }
}
